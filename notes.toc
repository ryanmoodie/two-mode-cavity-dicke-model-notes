\contentsline {part}{I\hspace {1em}Report preparation}{1}{part.1}
\contentsline {chapter}{\numberline {1}Notes}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Symmetries}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}States}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Regimes}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Spins}{2}{section.1.4}
\contentsline {chapter}{\numberline {2}Starting Stuff}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Equations}{3}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Dicke Hamiltonian}{3}{subsection.2.1.1}
\contentsline {subsubsection}{Original}{3}{section*.2}
\contentsline {subsubsection}{Avec primes}{3}{section*.3}
\contentsline {subsubsection}{Two mode}{3}{section*.4}
\contentsline {subsubsection}{Complex g's}{4}{section*.5}
\contentsline {subsubsection}{General}{4}{section*.6}
\contentsline {subsection}{\numberline {2.1.2}Lindblad}{5}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Mean Field Theory}{5}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Proto-EOM}{5}{subsection.2.1.4}
\contentsline {subsection}{\numberline {2.1.5}Semi-classical approximation}{6}{subsection.2.1.5}
\contentsline {subsection}{\numberline {2.1.6}Equations of Motion}{6}{subsection.2.1.6}
\contentsline {subsubsection}{Real}{6}{section*.7}
\contentsline {subsubsection}{Complex}{6}{section*.8}
\contentsline {subsection}{\numberline {2.1.7}EOM - $U_{a} \not =U_{b}$}{6}{subsection.2.1.7}
\contentsline {subsection}{\numberline {2.1.8}EOM - $S^{x}, S^{y}, S^{z}$}{7}{subsection.2.1.8}
\contentsline {subsubsection}{Real g}{7}{section*.9}
\contentsline {subsubsection}{Complex g}{7}{section*.10}
\contentsline {subsection}{\numberline {2.1.9}Steady states}{7}{subsection.2.1.9}
\contentsline {subsubsection}{Using plus, minus}{7}{section*.11}
\contentsline {subsubsection}{Using x, y}{8}{section*.12}
\contentsline {chapter}{\numberline {3}Methods}{9}{chapter.3}
\contentsline {section}{\numberline {3.1}Perturbation matrices}{9}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}General perturbation matrix}{9}{subsection.3.1.1}
\contentsline {subsubsection}{Using $S^{+}, S^{z}$}{9}{section*.13}
\contentsline {subsubsection}{Using $S^{x}, S^{y}, S^{z}$}{9}{section*.14}
\contentsline {chapter}{\numberline {4}Limiting cases}{11}{chapter.4}
\contentsline {section}{\numberline {4.1}Initial assumptions}{11}{section.4.1}
\contentsline {section}{\numberline {4.2}Case: $g^{\prime }=g$}{11}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Hamiltonian}{11}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}EOM}{11}{subsection.4.2.2}
\contentsline {subsubsection}{plus, minus}{11}{section*.15}
\contentsline {subsubsection}{x,y}{12}{section*.16}
\contentsline {subsection}{\numberline {4.2.3}Steady states}{12}{subsection.4.2.3}
\contentsline {subsubsection}{plus, minus}{12}{section*.17}
\contentsline {subsubsection}{x,y}{12}{section*.18}
\contentsline {section}{\numberline {4.3}Case: $g^{\prime }=0$}{12}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Hamiltonian}{12}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}EOM}{13}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Non-steady states (rotating solutions)}{13}{subsection.4.3.3}
\contentsline {chapter}{\numberline {5}Solutions}{14}{chapter.5}
\contentsline {section}{\numberline {5.1}Normal Phase}{14}{section.5.1}
\contentsline {section}{\numberline {5.2}Superradiant Phase}{14}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}$g^{\prime }=0$}{14}{subsection.5.2.1}
\contentsline {subsubsection}{Rotating Solution}{14}{section*.19}
\contentsline {subsection}{\numberline {5.2.2}$g^{\prime }=g$}{14}{subsection.5.2.2}
\contentsline {subsubsection}{SRA}{14}{section*.20}
\contentsline {subsubsection}{SRB}{14}{section*.21}
\contentsline {subsection}{\numberline {5.2.3}$g^{\prime }=i g$}{14}{subsection.5.2.3}
